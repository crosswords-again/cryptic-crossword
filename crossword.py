SIZE = 3
def get_columns(grid : list[str]) -> list[str] :
    columns = []
    for i in range(SIZE) :
        col = ''
        for j in grid :
            col += j[i]
        columns.append(col)
    return columns
    
def check_next(string: str, i: int) -> bool:
    if i < len(string) - 1 and string[i + 1] == "W" and string[i] != "B":
        return True
    return False
def check_previous(string: str, i: int) -> bool:
    if i > 0 and string[i - 1] == "B" and string[i] != "B":
        return True
    return False

# check box just checks within one string return the column number
def check_box(string: str) -> list[int,str]:
    n = len(string)
    result = list(string)
    for i in range(n):
        if i == 0:
            if check_next(string, i):
                result[i] = 'n'
            else:
                result[i] = '_'
        else:
            if check_next(string, i) and check_previous(string, i):
                result[i] = 'n'
            else:
                result[i] = '_'
    result1 = ''.join(result)
    return [(i, result1[i]) for i in range(len(result1))]

# numbering gives for the whole of across and whole of down which are list of strings
def numbering(rows : list[str]) -> list[str]:
    result = []
    for i in rows:
        result += check_box(i)
    return result

''' i dont know if we need this we might
def crossword(grid : list[str]) -> list[str]:
    columns = get_columns(grid)
    rows = grid
    return [numbering(columns),numbering(rows)]
'''
